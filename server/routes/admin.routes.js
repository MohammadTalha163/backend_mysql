//Get Dependencies
const express = require('express');
const router = express.Router();
// include Middlewares
const adminMiddleware= require("../middleware/admin.middleware");
//Include Controllers
const adminController= require("../controllers/admin.controller");


router.post('/signup',adminController.addAdmin);
router.post('/engineeridcheck',adminController.engineeridcheck);
router.post('/engContain',adminController.engContain);
router.get('/list',adminController.viewAdmin);
router.get('/engLimit',adminController.engLimit);


router.get('/test', function (req, res) {
    res.status(200).json({
        status: true,
        message: 'Working!'
    });
});


module.exports= router;
